var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var webserver = require('gulp-webserver');

var config = {
    source: './src/',
    sass: {
        include: 'sass/bootstrap.scss',
        destination: 'css'
    }
};

gulp.task('sass', function () {
    return gulp.src(config.source + config.sass.include)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write({
            includeContent: true
        }))
        .pipe(gulp.dest(config.source + config.sass.destination));
});

gulp.task('webserver', function() {
    gulp.src(config.source)
        .pipe(webserver());
});

gulp.task('watch', function() {
    gulp.watch(config.source + 'sass/**/*.scss', ['sass']);
});

gulp.task('default', ['sass']);