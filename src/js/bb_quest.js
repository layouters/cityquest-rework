var QuestView = Backbone.View.extend({
    tagName: 'tr',
    className: 'quests-row',
    template: _.template(
            '<td>' +
            '<%= title %> <a href="/quest/update?id=<%= id %>&type=<%= object_type%>" target="_blank">#</a>' +
            /*'<small><br>(<%= price_am %>-<%= price_pm %>)'+
             '<br>(<%= price_weekend_am %>-<%= price_weekend_pm %>)</small>'+*/
            '</td>' +
            '<td class="bb_times"></td>'),
    initialize: function () {
        this.render();

    },
    render: function () {
        var self = this;
        this.$el.html(this.template(this.model.attributes));
        return this;
    }
})

var Quest = Backbone.Model.extend({
    constructor: function () {
        Backbone.Model.apply(this, arguments);
    },
    defaults: {
        'haspopover': false
    },
    initialize: function (options) {
        var self = this;
        this.view = new QuestView({model: this});
        this.deferred = $.Deferred();
        this.bookings = options.bookings || [];
        this.socials = options.socials || [];
        this.city_id = options.city_id || [];
        var seanses = options.seanses || [];
        this.seances = new Seances([], {
            quest: this
        });

        if (seanses.length > 0) {
            $.each(seanses, function () {
                var seans = new Seance({time: String(this)});
                self.seances.add(seans);
            });
        }
        self.seances.render(this);
    },
    initBookings: function(active_day){
        var q = this;
        var bookings = new Bookings([], {
            quest:q,
            day:active_day
        });
        
        $.each(this.bookings, function(){
            var aQuest =  this;
            $.each(aQuest, function(){
                var quest = parseInt(this.quest_id);
                    quest = quest ? quest : parseInt(this.action_id);
                    quest = quest ? quest : parseInt(this.performance_id);
                    quest = quest ? quest : parseInt(this.vr_id);

                if(this.date == active_day.get('ymd') && quest == q.get('id')){
                    bookings.add( this );
                }
            });
        });
        bookings.setupBookings();
    },
    setPrice: function (active_day) {
//        console.log("Setting prices for " + active_day.get('ymd'));
        var q = this,
            promodays = q.collection.app.days.promodays,
            promoDay = promodays.find(function(model){
                    return parseInt(model.get('quest_id')) == parseInt(q.id) &&
                    model.get('ymd') == active_day.get('ymd');
            });

        this.seances.day = active_day;
        this.seances.each(function (seance) {
            var hour = parseInt(seance.get('time').split(':')[0]);
            var minut = parseInt(seance.get('time').split(':')[1]);

            var price_weekend_am = q.get('price_weekend_am'),
                price_weekend_pm = q.get('price_weekend_pm'),
                price_am = q.get('price_am'),
                price_pm = q.get('price_pm');
                //console.log(q.get('price_schedule_workday'));
            if (typeof (promoDay) != 'undefined') {

                if (hour > 9 && (hour < 17 || (hour == 17 && minut < 31))) {
                    seance.set('price', promoDay.get('price_am'));
                } else {
                    seance.set('price', promoDay.get('price_pm'));
                }


            }else if ((q.get("object_type") == "action") || (q.get("object_type") == "performance") || (q.get("object_type") == "quest") || (q.get("object_type") == "vr")) {

                //Scheduling 2.0
                var price_schedule = q.get('price_schedule_workday');
                if ((active_day.get('weekend') || active_day.get('holiday')))
                    price_schedule = q.get('price_schedule_weekend');

                //parsePriceAndSchedule described in the top of admin_main.js.

                var result_day_schedule = parsePriceAndSchedule(price_schedule);

                seance.set('price', result_day_schedule[seance.get('time')]);
            }
            else if ((q.get("object_type")!= "action") && (active_day.get('weekend') || active_day.get('holiday'))) {
                if (hour > 9 && (hour < 17 || (hour == 17 && minut < 31))) {
                    seance.set('price', price_weekend_am);
                } else {
                    seance.set('price', price_weekend_pm);
                }

            } else {
                if (q.get("object_type")== "action"){

                    if (active_day.get('weekend') || active_day.get('holiday')) {
                            seance.set('price', price_weekend_pm);
                    }else {

                        if (seance.get('time') >= '17:30') {
                            seance.set('price', price_pm);
                        } else {
                            seance.set('price', price_am);
                        }
                        if (seance.get('time') == '00:00') {
                            seance.set('price', 5000);
                        }

                        if (active_day.get('dayOfTheWeek') == 'Чт' || active_day.get('dayOfTheWeek') == 'Пт') {
                            if (seance.get('time') >= '17:30')
                                seance.set('price', price_weekend_am);
                        }
                    }

                } else {
                    if (hour > 9 && (hour < 17 || (hour == 17 && minut < 31))) {
                        seance.set('price', price_am);
                    } else {
                        seance.set('price', price_pm);
                    }
                }

            }
            if(q.collection.app.days.cities[q.get('city_id')] && currencies[q.collection.app.days.cities[q.get('city_id')]['country']]) {
                seance.set('unit', currencies[q.collection.app.days.cities[q.get('city_id')]['country']]);
            } else {
                seance.set('unit', currencies['ru']);
            }

            //if (hour > 2 && hour < 9) {
            //    seance.view.$el.hide();
            //}
        });

        q.initBookings(active_day);
    },
});

var Quests = Backbone.Collection.extend({
    model: Quest,
    initialize: function (models, options) {
        this.app = options.app;
        this.deferred = $.Deferred();
    },
    render: function () {
        var quests = $('#bb_quests tbody').html('');
        
        this.each(function (model) {
            if (model.get('status') == '2') {
                quests.append(model.view.el);
            }
        });
    },
    setPrice: function () {
        var active_day = this.app.days.getActiveDate();

        this.each(function (quest) {
            quest.setPrice(active_day);
        });
    },
    parse: function (response) {
        if (response && response.success) {
            return response.quests;
        } else {
            return false;
        }
    }
});