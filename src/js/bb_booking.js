var Booking = Backbone.Model.extend({
    initialize: function () {
        //console.log(this);
    }
});

var Bookings = Backbone.Collection.extend({
    model: Booking,
    initialize: function (models, options) {
        this.quest = options.quest;
        this.day = options.day;
    },
    setupBookings: function () {
        var q = this.quest;

        q.seances.each(function (s) {
            s.set('booking', false);
            s.booking = false;
        });

        this.each(function (model) {
            var seance = q.seances.find(function (s) {
                return s.get('time') == model.get('time');
            });

            if (seance) {
                seance.booking = model;
                seance.set('booking', true);
            }
        });
    },
});