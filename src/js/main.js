var asite = 'cityquest.ru',
        order_id = 0,
        price = 0;
var authAction = '';
var eventAction = '';

var stripHTML = function(str){
        var tmp = document.createElement('DIV');
        tmp.innerHTML = str;
        return tmp.textContent || tmp.innerText || "";
    };
    
var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
window.mobilecheck = function () {
    var check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
            check = true
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};
var Sliders = {};
(function($){
    
    var QuestSlider = {
        def: {
            container: '#carouselPhoto',
            slides: '.carousel-inner',
            slide: '.item',
            paginator: '.carousel-indicators',
            paginators: 'li',
            speed: 600,
            delay: 5000,
        },
        container: null,
        slides: null,
        slide: null,
        paginator: null,
        paginators: null,
        current: 0,
        blocked: false,
        lt: null,
        isAuto: true,
        id: null,
        init: function (options) {
            this.id = "slider" + (new Date().getTime());
            Sliders[this.id] = this;
//            console.log(Sliders);
            var foundContainer = false, 
                self = Sliders[this.id];
            self.container = options && options.hasOwnProperty('container') ? options.container : self.def.container;
            self.slides = options && options.hasOwnProperty('slides') ? options.slides : self.def.slides;
            self.slide = options && options.hasOwnProperty('slide') ? options.slide : self.def.slide;
            self.paginator = options && options.hasOwnProperty('paginator') ? options.paginator : self.def.paginator;
            self.paginators = options && options.hasOwnProperty('paginators') ? options.paginators : self.def.paginators;
            self.speed = options && options.hasOwnProperty('speed') ? options.speed : self.def.speed;
            self.delay = options && options.hasOwnProperty('delay') ? options.delay : self.def.delay;
            self.isAuto = options && options.hasOwnProperty('isAuto') ? options.isAuto : self.def.isAuto;
            var parents = $(self.slides).parents();
            

            if ($(self.container).size() > 0) {
                $.each(parents, function () {
                    if ($(this).attr("id") == $(self.container).attr("id")) {
                        foundContainer = true;
                    }
                });
            }

            if (foundContainer) {
                self.bindEvents();
                $(self.container).find(self.slides).find(".item").each(function () {
                    $(this).addClass("next")
                });
                $(self.container).find(self.slides).find(".item:eq(0)").each(function () {
                    $(this).removeClass("next")
                });
            }
            
            $(self.container).find(this.paginator).find("li:eq(0)").addClass("active");
        },
        slideTo: function (num) {
            var self = Sliders[this.id];
            var prev =self.current;
            if (prev == num || self.blocked)
                return;
            
            self.blocked = true;
            self.lt = new Date().getTime();
            setTimeout(function () {
                self.blocked = false;
            }, self.speed);
            
            $(self.container).find(self.slide).removeClass("prev next active");
            $(self.container).find(self.slide + ":eq(" + num + ")").addClass("active");

            //indicators
            $(self.paginator).find("li").removeClass("active");
            $(self.paginator).find("li:eq(" + num + ")").addClass("active");

            //slides
            if (num > prev) {
                $(self.container).find(self.slide + ":eq(" + prev + ")").addClass("prev").removeClass("next active");
                $(self.container).find(self.slide + ":eq(" + num + ")").addClass("active").removeClass("prev next");
            } else if (num < prev) {
                $(self.container).find(self.slide + ":eq(" + prev + ")").addClass("next").removeClass("prev active");
                $(self.container).find(self.slide + ":eq(" + num + ")").addClass("active").removeClass("prev");
            }
            
            $(self.container).find(self.slide).each(function (i) {
                if (i < num) {
                    $(this).removeClass("active prev next").addClass("prev");
                }
                if (i > num) {
                    $(this).removeClass("active prev next").addClass("next");
                }
            });
            self.current = num;
        },
        bindEvents: function () {
            var self = Sliders[this.id];
            $(self.container).find(self.paginator).find("li").unbind("click").bind("click", function () {
                if (!self.blocked)
                    self.slideTo($(this).index());
            });
            $(self.container).find(self.slides).on("swiperight", function () {
                if (0 <= self.current - 1) {
                    self.slideTo(self.current - 1);
                }
            });
            $(self.container).find(self.slides).on("swipeleft", function () {
                var count = $(self.container).find(self.slides).find(self.slide).size();

                if (count > self.current + 1) {
                    self.slideTo(self.current + 1);
                }
            });
            
            if(self.isAuto){
                self.lt = new Date().getTime();
                setInterval(function () {
                    var now = new Date().getTime();
                    if (self.lt + self.delay > now)
                        return;

                    var count = $(self.container).find(self.slide).size();
                    if (count > self.current + 1) {
                        self.slideTo(self.current + 1);
                    } else {
                        self.slideTo(0);
                    }

                }, 500);
            }
        },
    };
    
    window.QuestSlider = QuestSlider;
})(jQuery);


(function ($) {
    $.extend($.fx.step, {
        backgroundPosition: function (fx) {
            if (fx.pos === 0 && typeof fx.end == 'string') {
                var start = $.css(fx.elem, 'backgroundPosition');
                start = toArray(start);
                fx.start = [start[0], start[2]];
                var end = toArray(fx.end);
                fx.end = [end[0], end[2]];
                fx.unit = [end[1], end[3]];
            }
            var nowPosX = [];
            nowPosX[0] = ((fx.end[0] - fx.start[0]) * fx.pos) + fx.start[0] + fx.unit[0];
            nowPosX[1] = ((fx.end[1] - fx.start[1]) * fx.pos) + fx.start[1] + fx.unit[1];
            fx.elem.style.backgroundPosition = nowPosX[0] + ' ' + nowPosX[1];
            function toArray(strg) {
                strg = strg.replace(/left|top/g, '0px');
                strg = strg.replace(/right|bottom/g, '100%');
                strg = strg.replace(/([0-9\.]+)(\s|\)|$)/g, "$1px$2");
                var res = strg.match(/(-?[0-9\.]+)(px|\%|em|pt)\s(-?[0-9\.]+)(px|\%|em|pt)/);
                return [parseFloat(res[1], 10), res[2], parseFloat(res[3], 10), res[4]];
            }
        }
    });
})(jQuery);


(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'));
    } else {
        factory(jQuery);
    }
}(function ($) {
    var pluses = /\+/g;
    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }
    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }
    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }
    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }
        try {
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch (e) {
        }
    }
    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }
    var config = $.cookie = function (key, value, options) {
        if (arguments.length > 1 && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);
            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setTime(+t + days * 864e+5);
            }
            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }
        var result = key ? undefined : {};
        var cookies = document.cookie ? document.cookie.split('; ') : [];
        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            var name = decode(parts.shift());
            var cookie = parts.join('=');
            if (key && key === name) {
                result = read(cookie, value);
                break;
            }
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }
        return result;
    };
    config.defaults = {};
    $.removeCookie = function (key, options) {
        if ($.cookie(key) === undefined) {
            return false;
        }
        $.cookie(key, '', $.extend({}, options, {expires: -1}));
        return !$.cookie(key);
    };
}));

$(document).ready(function(){
    $('.ico-pay').tooltip();

    if ($('body').hasClass('video')) {
        var videoIsSet = false;
        function set_video_bgr() {
            if (document.body.clientWidth > 1023) {
                if(videoIsSet) return;
                var w = $('.jumbotron').outerWidth();
                var $video = $('<div id="video_container" style="display:none">' +
                        '<video autoplay="autoplay" id="bgr_video" loop="loop"></video></div>')
                        .prependTo('body.video');

                video = document.getElementById('bgr_video');
                video.addEventListener('loadeddata', function () {
                    $('.jumbotron').css('backgroud', 'none');
                    $video
                            .css({
                                width: '100%',
                                height: $('.jumbotron').outerHeight(),
                                overflow: 'hidden',
                                position: 'absolute',
                            })
                            .fadeIn(2000)
                            .find('video')
                            .css({
                                width: '100%',
                                height: 'auto',
                                display: 'block',
                            });
                }, false);

                video.src = '/img/fog_bg3_1.mp4';
                video.load();
                $("body").addClass("inner");
                videoIsSet=true;
            } else {
                var posX = 0, posY = 0;
                //$("body").removeClass("inner");
                videoIsSet=false;
            }

        }
        set_video_bgr();
        
        $(window).resize(function(){
            set_video_bgr();
        });
    }
    
    
    $('#bookgift-comment').elastic();

    var supportsOrientationChange = "onorientationchange" in window,
            orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

    window.addEventListener(orientationEvent, function () {
        if (document.body.clientWidth < 1025 && document.body.clientWidth > 767) {
            location.reload();
        }
    });


    $('#show-menu').click(function () {

        if ((document.body.clientWidth < 1025 && document.body.clientWidth > 980) || document.body.clientWidth < 768) {
//            if ($('#for-select-city').text() == '') {
//                $('.city-select').show().appendTo('#for-select-city');
                // $('#for-city').html('');
//            }

            if (document.body.clientWidth < 768) {
//                if ($('#for-login').text() == '') {
//                    $('#for-login-pl .btn').show()
//                            .css('display', 'inline-block')
//                            .appendTo('#for-login');
//
//                    $('#for-login-pl').html('');
//                }
            }

        } else {
//            if ($('#for-login-pl').text() == '') {
//                $('#for-login .btn').appendTo('#for-login-pl');
//                // $('#for-login').html('');
//                if (document.body.clientWidth < 768) {
//                    $('#for-login-pl .btn').hide();
//                }
//            }
//            if ($('#for-city').text() == '') {
//                $('.city-select').hide().appendTo('#for-city');
//                // $('#for-select-city').html('');
//            }
        }

        if ($('#myModalMenu #for-menu').text() == '') {
            $('#top_menu').show().appendTo('#myModalMenu #for-menu');

            var host = window.location.host;
            var sda = host.split(".");
//            console.log(sda[0]);
//            if(sda[0].indexOf('cityquest') > -1){
            setTimeout(function () {
                $('#myModalMenu #for-menu').find("#second_nav").show();
            }, 50);

//            }


        }

        if (document.body.clientWidth < 1025 && document.body.clientWidth > 991) {
            $('#second_nav').show();
        }

        $('#myModalMenu').modal('toggle');
    });

    $('#myModalMenu').on('hidden.bs.modal', function (e) {
        if (document.body.clientWidth < 1025 && document.body.clientWidth > 980) {
            $('#top_menu').appendTo('#top_menu_container');
            $('#top_menu #second_nav').hide();
        } else {
            $('#top_menu #second_nav').show();
        }
    });


    $('.curent_date').click(function () {
        $('.curent_date').removeClass('active');
        $(this).addClass('active');
    });


    if (window.mobilecheck()) {

        $('.calendar_container').css('overflow', 'auto');


        $('.move-right').click(function () {
            var step = $('.calendar_container').width();
            $(".calendar_container").scrollTo('+=' + step, 300);
        });


        $('.move-left').click(function () {
            var step = $('.calendar_container').width();
            $(".calendar_container").scrollTo('-=' + step, 300);
        });

    } else {

        $('.move-right').click(function () {

            var step = $('.calendar_container').width(),
                    ml = parseInt($('.calendar').css('margin-left')) - step,
                    sum_w = 0;

            $.each($('.curent_date'), function (i, el) {
                sum_w += $(el).outerWidth();
            });

            if (-sum_w <= ml)
                $('.calendar').animate({'margin-left': '-=' + step});
        });

        $('.move-left').click(function () {
            var step = $('.calendar_container').width(),
                    ml = parseInt($('.calendar').css('margin-left'));

            if (ml < 0)
                $('.calendar').animate({'margin-left': '+=' + step});
        });
    }

    $('#myModalBook').on('show.bs.modal', function (e) {
        if (user_name && user_name != '') {
            return true;
        } else {
            $('#myModalAuth').modal('show');
            return false;
        }
    });

    $('#myModalBook').on('shown.bs.modal', function (e) {

        if (document.body.clientWidth > 767) {
            var h = $('#myModalBook .img-responsive').height();
            $('.shad').height(h);
        }

//        if (typeof yaCounter25221941 != 'undefined')
//            yaCounter25221941.reachGoal('openBookWindow');
        if (typeof ga != 'undefined')
            ga('gtm1450431798761.send', 'event', 'book', 'openWindow');
    });

    
    $('#myModalAuth .modal-title').click(function () {
        $('#myModalAuth .modal-title').removeClass('active');
        $(this).addClass('active');
        authAction = $(this).attr("id") == 'auth-tab' ? 'login' : 'register';
    });
    
    
    
    $(".autu-vk").click(function(){
        eventAction = 'vkontakte';
    });
    
    var ModalBook = $('#myModalBook'), book_data = 0, btn_time;
    var ModalAfterBuy = $('#myModalAfterBuy');
    var ModalCancel = $('#ModalCancel');
    
    ModalAfterBuy.on('shown.bs.modal', function (e) {
        $(".modal-backdrop").click(function(){
            ModalAfterBuy.modal('hide');
        });
    });
    ModalCancel.on('shown.bs.modal', function (e) {
        $(".modal-backdrop").click(function(){
            ModalCancel.modal('hide');
        });
    });
    
    var hideBookingImage = function(){
        if($(window).height() <= 480 ){
            $("#myModalBook .featurette-image").css({display:"none!important"});
        } else { 
            $("#myModalBook .featurette-image").css({display:"block!important"});
        }
    };
    hideBookingImage();
    $(window).resize(function(){hideBookingImage()});
    
    $('.btn.btn-q, .full-schedule .seance').not('.busy').click(function (e) {
        
        if (user_phone == '00000' || user_phone == '000000' || user_phone == '0000000' || user_phone == '00000000') {
            user_phone = '';
        }
        $('.you_phone input', ModalBook).mask('+7(000) 000-00-00', {placeholder: "+7(___) ___-__-__"})
                .val(user_phone)
                ;

        btn_time = $(e.target);
        var isaction = (window.location.pathname.split('/')[1] == 'action');
        //alert(isaction);
        if(!quest_type) {
            quest_type = btn_time.attr('data-quest-type').toLowerCase();
        }

        if(!quest_name) {
            quest_name = btn_time.attr('data-quest-name').toLowerCase();
        }

        book_data = {
            quest_id: btn_time.attr('data-quest'),
            quest_cover: btn_time.attr('data-quest-cover'),
            address: $('.addr-quest span').text() || $('#quest_addr_' + btn_time.attr('data-quest')).val(),
            title: btn_time.attr('data-quest-name') || $('#quest_title').text() || $('#quest_title_' + btn_time.attr('data-quest')).text() || $(".text-container-action > h2").text(),
            day: btn_time.attr('data-day'),
            ymd: btn_time.attr('data-ymd'),
            date: btn_time.attr('data-date'),
            d: btn_time.attr('data-d'),
            m: btn_time.attr('data-m'),
            time: btn_time.attr('data-time'),
            price: btn_time.attr('data-price'),
            phone: $('.you_phone input').val(),
            name: user_name,
            comment: ' ',
            result: '',
            isFrontBooking: true
        };

        $.cookie("last_seance_click_date", btn_time.attr('data-ymd'));
        $.cookie("last_seance_click_time", btn_time.attr('data-time'));
        
        if (isaction || quest_type == 'action') {
            delete book_data['quest_id'];
            book_data['action_id'] = btn_time.attr('data-quest');
            ModalBook.addClass('is_action');
        }
        if(quest_type == 'quest'){
            $('.featurette-image', ModalBook).css({
                'background-image': "url('/images/" + book_data.quest_cover + "')"
            });
        } else {
            $('.featurette-image', ModalBook).css({
                'background-image': "url('/images/" + quest_type + "s/cover/"+ book_data.quest_cover + "')"
            });
        }
        var mArr = window.i18n.months;
        $('.addr-to', ModalBook).html('<i class="ico-loc iconm-Pin"></i>' + book_data.address);
        $('.h2', ModalBook).html(book_data.title);
        $(".book_time", ModalBook).find('.date').text(book_data.day + ", " + book_data.date);
        $(".book_time", ModalBook).find('.time').text(book_data.time);
        
//        $('.book_time', ModalBook).html(
//                '<small>' + book_data.day + '</small>' +
//                '<span>' + book_data.d + '.' + book_data.m + '</span><em>в</em><span>' + book_data.time + '</span>');

        var hostname = window.location.hostname.split('.');
        var currency = $("<div />").append($("<span class='icon icon-currency'></span>")).html();
        
        if (hostname[hostname.length - 1] == 'kz') {
            currency = $("<div />").append($(".currency").clone()).html();
        }

        $('.price', ModalBook).html('<span class="price_content">'+formatMoney(book_data.price, 0,'.','&nbsp;')+'</span>' + currency);

        ModalBook.modal('show');
        $('.is_action .players_count-select button .dropdown-content').html(4);
        $('.is_action .players_count-hint').hide();
        $.data(ModalBook[0], 'book_data', book_data);
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            'event' : 'btnOrder',
            'event-action': quest_type,
            'event-label': quest_name
        });
    });
    
    $('.confirm', ModalBook).click(function (e) {
        
        if (user_name && user_name != '') {
            book_data.phone = $('.you_phone input').val(); // || user_phone
            book_data.players_count = $('.is_action .players_count-select button .dropdown-content').html() || '';

            var btn_book = $(e.target);
            if (book_data.phone == '00000' || book_data.phone == '' || book_data.phone.length != 17) {
                $('.you_phone input')
                        .attr({
                            'data-toggle': "tooltip",
                            'title': 'Необходимо указать корректный номер телефона',
                        }).tooltip('show');
            } else {
                if (book_data != 0) {
                    var pathArray = window.location.pathname.split('/');
                    var postUrl = "/booking/create";
                    if (pathArray[1] == "action")
                        postUrl = "/booking/createaction";
                    $('.confirm', ModalBook).attr('disabled', true);
                    $.post(postUrl,
                            book_data,
                            function (result) {
                                if (result && result.success) {
                                    if (typeof yaCounter25221941 != 'undefined'){
//                                        yaCounter25221941.reachGoal('confirmBook');
                                    }
                                    if (typeof ga != 'undefined')
                                        ga('gtm1450431798761.send', 'event', 'book', 'confirmBook');
                                    
                                    var quest_type_name = result.quest_type == 'vr'? 'Виртуальная реальность' :
                                            (result.quest_type == 'performance' ? 'Перфоманс': 
                                                (result.quest_type == 'action' ? 'Экшн' : 'Квест'));
                                    window.dataLayer = window.dataLayer || [];
                                    dataLayer.push({
                                        'event' : 'btnOrderConfirm',
                                        'event-action': result.quest_type,
                                        'event-label': result.quest_title,
                                        'ecommerce' : {
                                            'purchase' : {
                                                'actionField' : {
                                                    'id': "order"+result.id,
                                                    'revenue' : result.price
                                                },
                                                'products' : [ {
                                                    'name' : result.quest_title,
                                                    'id' : "quest"+result.model[result.quest_type+"_id"],
                                                    'price' : result.model.price,
                                                    'category' : quest_type_name,
                                                    'quantity' : 1
                                                }]
                                            }
                                        }
                                    });
                                    
                                    var order_id = result.id,
                                            uid = result.uid,
                                            client_id = result.client_id,
                                            price = book_data.price,
                                            currency_code = 'RUB',
                                            country_code = 'RU',
                                            hostname = window.location.hostname.split('.');

                                    if (hostname[hostname.length - 1] == 'kz') {
                                        var currency_code = 'KZT',
                                                country_code = 'KZ';
                                    }

                                    if (uid && uid != '') {

                                        var wh = $(window).height(),
                                                ww = $(window).width();

                                        var url = 'http:' + '//ad.admitad.com/r?postback=1&' +
                                                'postback_key=033BA5Ca6f9952713930e4e1336425E3&' +
                                                'uid=' + uid + '&' +
                                                'campaign_code=e8be9dc3d4&' +
                                                'action_code=1&' +
                                                'order_id=' + order_id + '&' +
                                                'tariff_code=1&' +
                                                'client_id=' + client_id + '&' +
                                                'screen=' + ww + 'x' + wh + '&' +
                                                'price=' + price + '&' +
                                                'currency_code=' + currency_code + '&' +
                                                'country_code=' + country_code + '&' +
                                                'payment_type=lead';

//                                        console.log(url);

                                        $.ajax({
                                            type: 'GET',
                                            url: url,
                                            // dataType: 'jsonp',
                                            success: function (data) {
//                                                console.log(data);
                                            }
                                        });
                                    }

                                    ModalBook.modal('hide');
                                    $('.confirm', ModalBook).attr('disabled', false);
                                    btn_time.attr({
                                        'disabled': 'disabled',
                                        'data-toggle': "tooltip",
                                        'data-delay': "4000",
                                        'title': 'Квест успешно забронирован',
                                    })
                                            .tooltip({
                                                delay: {show: 2000, hide: 3000}
                                            })
                                            .tooltip('show')
                                            .addClass('myDate');

                                    setTimeout(function () {
                                        btn_time.tooltip("hide");
                                    }, 3000);
                                   
                                    var qt = '', 
                                        qn = result.quest_title, 
                                        qd = '' +  result.model.date;
                                    
                                    qd = qd.substring(6,8) + '.' + qd.substring(4,6) + '.' +qd.substring(0,4);
                                   
                                    switch(result.quest_type){
                                        case 'action': qt = 'экшн';
                                            break;
                                        case 'performance': qt = 'перфоманс';
                                            break;
                                        case 'vr': qt = 'VR квест';
                                            break;
                                        default: qt = 'квест';
                                            break;    
                                    }
                                   
                                   ModalAfterBuy.find(".quest_type").text(qt);
                                   ModalAfterBuy.find(".quest_name").text(qn);
                                   ModalAfterBuy.find(".quest_date").text(qd + ' в ' + result.model.time);
                                   ModalAfterBuy.modal("show");
                                   
                                } else {

                                    if (result && result.message) {

                                        btn_book
                                                .attr({
                                                    'data-toggle': "tooltip",
                                                    'title': result.message,
                                                }).tooltip('show');
                                    }

                                    alert('Ошибка!' + "\r\n" + result.message);
                                }
                            }
                    );
                }
                else
                    console.log('пустой book_data');
            }
        } else {

            ModalBook.modal('hide');
            $('#myModalAuth').modal('show');

        }

        return false;

    });


    $('#reg-phone, .you_phone input, #edit-phone')
            .mask('+7(000) 000-00-00', {placeholder: "+7(___) ___-__-__"})
            .focus(function (e) {
//                if ($(e.target).val() == '')
//                    $(e.target).val('+7(');
            });

    $('#form-group-reg-email input').keypress(function () {
        $('#form-group-reg-email span').tooltip('destroy');
        $('#form-group-reg-email').removeClass('input-error');
    });

    $('#form-group-reg-name input').keypress(function () {
        $('#form-group-reg-name span').tooltip('destroy');
        $('#form-group-reg-name').removeClass('input-error');
    });

    $('#form-group-reg-phone input').keypress(function () {
        $('#form-group-reg-phone span').tooltip('destroy');
        $('#form-group-reg-phone').removeClass('input-error');
    });

    $('#form-group-reg-pass input').keypress(function () {
        $('#form-group-reg-pass span').tooltip('destroy');
        $('#form-group-reg-pass').removeClass('input-error');
    });

    $('#reg-form').submit(function () {

        $('#form-group-reg-pass, #form-group-reg-email, #form-group-reg-name, #form-group-reg-phone').removeClass('input-error');
        $('#form-group-reg-email span, #form-group-reg-name span, #form-group-reg-phone span, #form-group-reg-pass span, #reg-form button').tooltip('destroy');

        if ($('#reg-name').val() !== '' && $('#reg-name').val().length > 2) {
            if ($('#reg-email').val() !== '' && re.test($('#reg-email').val())) {
                if ($('#reg-phone').val() !== '' && $('#reg-phone').val().length == 17) {
                    if ($('#reg-pass').val() !== '' && $('#reg-pass').val().length > 4) {
                        //if ( $('#reg-rules').is(':checked') ) {

                        $.post(
                                "/user/registration",
                                {
                                    name: $('#reg-name').val(),
                                    email: $('#reg-email').val(),
                                    phone: $('#reg-phone').val(),
                                    pass: $('#reg-pass').val(),
                                },
                                function (data) {

                                    if (data.success && data.success == 1) {

//                                        yaCounter25221941.reachGoal('registrationSuccess');
                                        ga('gtm1450431798761.send', 'event', 'registration', 'registrationSuccess');
                                        
                                        window.dataLayer = window.dataLayer || [];
                                        dataLayer.push({
                                            'event' : 'btnReg',
                                            'event-action': 'form'
                                        });


                                        $('#reg-form button')
                                                .attr({'title': 'Вы успешно зарегистрировались'})
                                                .tooltip('show');

                                        setTimeout(function () {
                                            $('#reg-form button').tooltip('destroy');
                                            location.reload();
                                        }, 1000);


                                    }

                                    if (data && data.error && data.errors) {

                                        if (data.errors.email) {
                                            $('#form-group-reg-email').addClass('input-error');
                                            $('#form-group-reg-email span')
                                                    .attr({'title': data.errors.email.join(', ')})
                                                    .tooltip('show');
                                        }

                                        if (data.errors.username) {
                                            $('#form-group-reg-name').addClass('input-error');
                                            $('#form-group-reg-name span')
                                                    .attr({'title': data.errors.email.join(', ')})
                                                    .tooltip('show');
                                        }
                                    }

                                    $('#reg-form button')
                                            .attr({'title': 'Ошибка при регистрации'})
                                            .tooltip('show');
                                }
                        );

                    } else {

                        $('#form-group-reg-pass').addClass('input-error');
                        $('#form-group-reg-pass span')
                                .attr({'title': 'Пароль должен содержать более 4 символов'})
                                .tooltip('show');
                    }
                } else {

                    $('#form-group-reg-phone').addClass('input-error');

                    $('#form-group-reg-phone span')
                            .attr({'title': 'Некорректный или неуказан номер телефона'})
                            .tooltip('show');
                }
            } else {

                $('#form-group-reg-email').addClass('input-error');

                $('#form-group-reg-email span')
                        .attr({'title': 'Пустой или некорректный email'})
                        .tooltip('show');
            }
        } else {

            $('#form-group-reg-name').addClass('input-error');

            $('#form-group-reg-name span')
                    .attr({'title': 'Имя не может быть пустым и должно содержать менее 2 символов'})
                    .tooltip('show');
        }

        return false;
    });

    $('#form-group-username-auth input').keypress(function () {
        $('#auth-form button').tooltip('destroy');
        $('#form-group-username-auth span').tooltip('destroy');
        $('#form-group-username-auth').removeClass('input-error');
    });

    $('#form-group-pass-auth input').keypress(function () {
        $('#auth-form button').tooltip('destroy');
        $('#form-group-pass-auth span').tooltip('destroy');
        $('#form-group-pass-auth').removeClass('input-error');
    });

    var allowClick = true;

    $('#auth-form').submit(function () {

        $('#form-group-username-auth, #form-group-pass-auth').removeClass('input-error');

        $('#form-group-username-auth span').tooltip('destroy');
        $('#form-group-pass-auth span').tooltip('destroy');
        $('#auth-form button').tooltip('destroy');

        if ($('#auth-form button').attr('data-value') == 'go') {

            $('.info-message').fadeOut().find('p').html('');

            if ($('#auth-email').val() !== '') {
                if (re.test($('#auth-email').val())) {
                    $('#form-group-username-auth').removeClass('input-error');

                    if ($('#auth-pass').val() !== '') {

                        if ($('#auth-pass').val().length > 3) {

                            $.post("/user/login",
                                    {
                                        'UserLogin[username]': $('#auth-email').val(),
                                        'UserLogin[password]': $('#auth-pass').val()
                                    },
                            function (data) {
                                
                                if (data.error && data.error == 1) {

                                    if (data.msg && data.msg == 'Вы уже авторизованы!') {
                                        $('#myModalAuth').modal('hide');
                                        if(window.location.pathname.indexOf('/review/create/') > -1){
                                            location.reload();
                                        } else {
                                           if (window.location.pathname == '/user/login') {
                                                window.location.href = '/';
                                            } else {
                                                location.reload();
                                            } 
                                        }
                                        
                                    } else {
                                        $('.info-message').fadeIn().find('p').html('Неверный логин или пароль');
                                    }

                                } else {
                                    if (data.success && data.success == 1) {

                                        $('#auth-form button')
                                                .attr({'title': 'Вы успешно авторизовались'})
                                                .tooltip('show');


                                        setTimeout(function () {
                                            $('#myModalAuth').modal('hide');
                                            if(window.location.pathname.indexOf('/review/create/') > -1){
                                                location.reload();
                                            } else {
                                                if (data.admin > 0) {
                                                    window.location.href = '/quest/ajaxschedule/ymd';
                                                } else {
                                                    location.reload();
                                                }
                                            }
                                        }, 500);

                                        return true;

                                    }
                                }
                            }
                            );

                        } else {

                            $('#form-group-pass-auth').addClass('input-error');
                            $('#form-group-pass-auth span')
                                    .attr({'title': 'Пароль должен содержать более 3 символов'})
                                    .tooltip('show');
                        }

                    } else {

                        $('#form-group-pass-auth').addClass('input-error');
                        $('#form-group-pass-auth span')
                                .attr({'title': 'Пароль не может быть пустым'})
                                .tooltip('show');
                    }
                } else {
                    $('#form-group-username-auth').addClass('input-error');
                    $('#form-group-username-auth span')
                            .attr({'title': 'Некорректный Email'})
                            .tooltip('show');
                }
            } else {
                $('#form-group-username-auth').addClass('input-error');
                $('#form-group-username-auth span')
                        .attr({'title': 'Поле Email не может быть пустым'})
                        .tooltip('show');
            }

            // forgot password
        } else {

            $('#form-group-forgot-auth').removeClass('input-error');
            $('#form-group-forgot-auth span').tooltip('destroy');
            $('#auth-form button').tooltip('destroy');

            if ($('#auth-forgot').val() !== '') {

                if (re.test($('#auth-forgot').val())) {

                    if (allowClick) {
//                        console.log("allow? " + allowClick);
                        allowClick = false;
                        setTimeout(function () {
                            allowClick = true;
                        }, 5000);

                        $('.info-message').fadeOut().find('p').html('');

                        $.post("/user/recovery",
                                {'UserRecoveryForm[email]': $('#auth-forgot').val()},
                        function (result) {

                            if (result && result.error && result.error == 1) {

                                if (result.errors.email) {
                                    $('.info-message').hide();
                                    $('#form-group-forgot-auth').addClass('input-error');
                                    $('#form-group-forgot-auth span')
                                            .attr({'title': result.errors.email.join(', ')})
                                            .tooltip('show');
                                } else {
                                    $('.info-message').fadeIn().find('p').html(result.msg);
                                }
                            } else if (result && result.success && result.success == 1) {
                                $('.info-message').fadeIn().find('p').html(result.msg);
                            }
                        }
                        );

                    }




                } else {
//                    if(allowClick){
                    $('#form-group-forgot-auth').addClass('input-error');
                    $('#form-group-forgot-auth span')
                            .attr({'title': 'Некорректный Email'})
                            .tooltip('show');
//                    }
                }
            } else {
//                if(allowClick){
                $('#form-group-forgot-auth').addClass('input-error');
                $('#form-group-forgot-auth span')
                        .attr({'title': 'Поле Email не может быть пустым'})
                        .tooltip('show');
//                }
            }

        }

        return false;
    });

    var book_id = '', btn_decline = null;
    
    $('.decline-book').click(function (e) {
        btn_decline = $(e.target);
        book_id = btn_decline.attr('data-book-id');
        console.log("aaa");
        ModalCancel.modal("show");
    });
    
    ModalCancel.find('.answer-no').click(function(){
        book_id = '';
        btn_decline = null;
        ModalCancel.modal("hide");
    });
    
    ModalCancel.find('.answer-yes').click(function(){
        if(book_id.length < 1 || !btn_decline) {
            ModalCancel.modal("hide");
            return;
        }
        
        $.post('/booking/decline', {
            id: book_id
        }, function (result) {
            if (result && result.success) {

                btn_decline
                        .attr({
                            'data-toggle': "tooltip",
                            'title': 'Бронирование отменено',
                        })
                        .tooltip('show');


                $('#row_fade_' + book_id).animate({height: 0}, 600, function () {
                    $('#row_fade_' + book_id).remove();
                });
                $('#row_book_' + book_id).animate({height: 0}, 600, function () {
                    $('#row_book_' + book_id).remove();
                });
                
                ModalCancel.modal("hide");
            } else {

                btn_decline
                        .attr({
                            'data-toggle': "tooltip",
                            'title': 'Произошла ошибка, свяжитесь с администрацией',
                        })
                        .tooltip('show');
            }
        });
        
    });


    $('#myModalEditProfile #form-group-username input').keypress(function () {
        $('#myModalEditProfile #form-group-username span').tooltip('destroy');
        $('#myModalEditProfile #form-group-username').removeClass('input-error');

        $('#editProfile').tooltip('destroy');
    });

    $('#form-group-forgot-auth input').keypress(function () {
        $('#form-group-forgot-auth span').tooltip('destroy');
        $('#form-group-forgot-auth').removeClass('input-error');

        $('#auth-form button').tooltip('destroy');
    });


    $('#myModalEditProfile #form-group-phone input').keypress(function () {
        $('#myModalEditProfile #form-group-phone span').tooltip('destroy');
        $('#myModalEditProfile #form-group-phone').removeClass('input-error');

        $('#editProfile').tooltip('destroy');
    });


    $('#myModalEditProfile').on('shown.bs.modal', function () {
        $('#edit-name').val($('.cabinet .name').text());
        $('#edit-phone').val($('.cabinet .phone').text());
    });

    $('#form-group-origin-pass input').keypress(function () {
        $('#form-group-origin-pass span').tooltip('destroy');
        $('#form-group-origin-pass').removeClass('input-error');

        $('#editProfile').tooltip('destroy');
    });

    $('#form-group-new-pass input').keypress(function () {
        $('#form-group-new-pass span').tooltip('destroy');
        $('#form-group-new-pass').removeClass('input-error');

        $('#editProfile').tooltip('destroy');
    });

    $('#form-group-new-confirm-pass input').keypress(function () {
        $('#form-group-new-confirm-pass span').tooltip('destroy');
        $('#form-group-new-confirm-pass').removeClass('input-error');

        $('#editProfile').tooltip('destroy');
    });
    
    
    $('#edit-form').submit(function () {

        // смена пароля
        if ($('#edit-pass').hasClass('active')) {

            if ($('#form-group-origin-pass input').val() != '' && $('#form-group-origin-pass input').val().length > 3) {

                if ($('#form-group-new-pass input').val() != '' && $('#form-group-new-pass input').val().length > 3) {

                    if ($('#form-group-new-pass input').val() == $('#form-group-new-confirm-pass input').val()) {

                        $.post(
                                '/user/profile/changepassword',
                                {
                                    'UserChangePassword[password]': $('#form-group-new-pass input').val(),
                                    'UserChangePassword[verifyPassword]': $('#form-group-new-confirm-pass input').val(),
                                    'UserChangePassword[oldpassword]': $('#form-group-origin-pass input').val(),
                                },
                                function (result) {
                                    if (result && result.success && result.success == 1) {
                                        $('#editProfile')
                                                .attr({'title': result.message})
                                                .tooltip('show');

                                        setTimeout(function () {
                                            $('#myModalEditProfile').modal('hide');
                                            $('#form-group-new-pass input').val("");
                                            $('#form-group-new-confirm-pass input').val("");
                                            $('#form-group-origin-pass input').val("");
                                            $('#editProfile').tooltip('destroy');
                                        }, 2000);
                                    }
                                }
                        );


                    } else {

                        $('#form-group-new-confirm-pass').addClass('input-error');
                        $('#form-group-new-confirm-pass span')
                                .attr({'title': 'Пароли не совпадают'})
                                .tooltip('show');
                    }


                } else {

                    $('#form-group-new-pass').addClass('input-error');
                    $('#form-group-new-pass span')
                            .attr({'title': 'Новый пароль должен содержать более 4 символов'})
                            .tooltip('show');
                }


            } else {

                $('#form-group-origin-pass').addClass('input-error');
                $('#form-group-origin-pass span')
                        .attr({'title': 'Пароль должен содержать более 4 символов'})
                        .tooltip('show');

            }

            // смена имени и телефона
        } else {
            
            var editName = stripHTML($('#edit-name').val());
            var editPhone = stripHTML($('#edit-phone').val());
    
            if (editName !== '' && editName.length > 2) {
                if (editPhone !== '' && editPhone.length == 17) {

                    var name = editName,
                        btn_edit = $('#editProfile'),
                        phone = editPhone;
                    
                    $.post('/user/profile/edit', {
                        username: name,
                        phone: phone,
                    }, function (result) {

                        if (result && result.success) {

                            btn_edit
                                    .attr({'title': result.message})
                                    .tooltip('show');

                            $('.cabinet .name').html(name);
                            $('.cabinet .phone').html(phone);
                            $('.you_phone input').val(phone);

                            setTimeout(function () {
                                $('#myModalEditProfile').modal('hide');
                                $('#editProfile').tooltip('destroy');
                            }, 2000);

                        } else {

                            btn_edit
                                    .attr({
                                        'data-toggle': "tooltip",
                                        'title': 'Произошла ошибка, свяжитесь с администрацией',
                                    })
                                    .tooltip('show');

                            if (result && result.errors) {
                                if (result.errors.username) {
                                    $('#form-group-username').addClass('input-error');
                                    $('#form-group-username span')
                                            .attr({'title': result.errors.username.join(', ')})
                                            .tooltip('show');
                                }
                                if (result.errors.phone) {
                                    $('#form-group-phone').addClass('input-error');
                                    $('#form-group-phone span')
                                            .attr({'title': result.errors.phone.join(', ')})
                                            .tooltip('show');
                                }
                            } else {

                                btn_edit
                                        .attr({
                                            'data-toggle': "tooltip",
                                            'title': 'Произошла ошибка, свяжитесь с администрацией',
                                        })
                                        .tooltip('show');
                            }

                        }
                    });

                } else {

                    $('#myModalEditProfile #form-group-phone').addClass('input-error');

                    $('#myModalEditProfile #form-group-phone span')
                            .attr({'title': 'Номер телефона должен содержать 10 символов'})
                            .tooltip('show');
                }

            } else {

                $('#myModalEditProfile #form-group-username').addClass('input-error');

                $('#myModalEditProfile #form-group-username span')
                        .attr({'title': 'Имя не может быть пустым и должно содержать менее 2 символов'})
                        .tooltip('show');

            }
        }


        return false;
    });


    /* ВОССТАНОВИТЬ ПАРОЛЬ */
    $('#forgot').click(function () {
        $(this).hide();
        $('#auth_toogl').show();

        $('.info-message').fadeOut().find('p').html('');

        $('#form-group-username-auth, #form-group-pass-auth').hide();
        $('#form-group-forgot-auth').show();

        $('#myModalAuth button.btn').html('ВОССТАНОВИТЬ').attr('data-value', 'forgot');
    });

    $('#auth_toogl').click(function (event) {
        $('#forgot').show();
        $(this).hide();

        $('.info-message').fadeOut().find('p').html('');

        $('#form-group-username-auth, #form-group-pass-auth').show();
        $('#form-group-forgot-auth').hide();


        $('#myModalAuth button.btn').html('войти').attr('data-value', 'go');
    });

    var winners_year_selector = function (link, target) {

        if (typeof target !== typeof undefined &&
                typeof quest_type !== typeof undefined &&
                typeof link !== typeof undefined &&
                typeof quest_id !== typeof undefined) {

            var initHeight = $(".btn-group-month").get(0).clientHeight;
            if (initHeight == 0)
                initHeight = 'auto';
            $(".btn-group-month").height(initHeight);

            var status = false;
            $(".btn-group-month").on("click", "ul.years", function () {
                $(this).css({'overflow': 'initial'}).addClass("opened");
                $(this).closest(".btn-group-month").height($(this).closest(".btn-group-month").get(0).clientHeight);
                $(this).find("li").show();
//                console.log("a");
                status = true;
            });
            $(".btn-group-month").on("mouseleave", "ul.years", function (e) {
                setTimeout(function () {
                    if ($(".btn-group-month").find("ul.years").hasClass("opened") && !status)
                        return;
                    status = false;
                    $(".btn-group-month").find("ul.years").removeClass("opened");
                    $(".btn-group-month").find("ul.years li:not(.active)").hide();
                }, 200);


            });
            $(".btn-group-month").on("click", "ul.years li", function () {
                if (!$(this).parent().hasClass("opened") || $(this).hasClass("active"))
                    return;
                var year = $(this).text();
                $.get("/" + quest_type + "/" + link + "/" + quest_id, {year: year}, function (res) {
                    $("#" + target).html(res);
                    winners_year_selector(link, target);
                });
            });
        }
    };

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var link = $(this).data("link") || '';
        target = $(this).attr("href").split("#")[1];

        $('a[data-toggle="tab"]').removeClass('active');
        $(e.target).addClass('active');
        
        if (link.length > 0) {
            if (typeof quest_type === typeof undefined || typeof quest_id === typeof undefined)
                return;
            
            if ($.trim($("#" + target).html()).length == 0) {
                $(".preloader").show();
                $.get("/" + quest_type + "/" + link + "/" + quest_id, {}, function (res) {
                    $(".preloader").hide();
                    $("#" + target).html(res);
                    
                    switch(target){
                        case 'rating': fixRatingMostPhotos(); break;
                        case 'schedule': winners_year_selector(link, target); 
                        case 'winner': winners_year_selector(link, target); 
                            break;
                        case 'reviews':
                            var ReviewSlider = jQuery.extend(true, {}, QuestSlider);
                            ReviewSlider.init({
                                container:'#reviews',
                                paginator: '.indicators-review',
                                isAuto:false,
                            }); 
                        default: break;
                    }
                });
            }
            
            var maxHeight = 0;
            $('.rating_photos:not(.most) .border').each(function () {
                if (maxHeight < $(this).height())
                    maxHeight = $(this).height();
            });
            if (maxHeight > 0) {
                $('.rating_photos:not(.most) .border').height(maxHeight);
            }
            changeSize();

        }
        
        fixHeightModalbook();
    });

    // ratings most photos
    var changeSize = function () {
        var borders = $(".rating_photos.most").find(".border img");
        var h = 0;
        $.each(borders, function () {
            h = $(this).get(0).clientHeight > h ? $(this).get(0).clientHeight : h;
        });
        if ($(window).width() < 992) {
            $(".the_first").show();
            $(".top_1.looped").hide();
        } else {
            $(".the_first").hide();
            $(".top_1.looped").show();
        }
        ;
        $(".rating_photos.most").find(".thumbnail.thumbnail-transp").css("max-height", h - h / 5 - 8);
    };
    $(window).resize(function () {
        changeSize();
    });


    $('#bookgift-phone').mask('+7(000) 000-00-00', {placeholder: "+7(___) ___-__-__"});

    /* PRESENT CARDS*/
    var preventMultipleClicks = false;

    if ($(".giftcards").size() > 0) {

        if (typeof msg !== typeof undefined && $.trim(msg).length > 0 && status_send) {
            alert(msg);
        }

        var holder = $(".giftcards"),
                cart = $(".giftcards_cart"),
                car_items = cart.find(".order_items"),
                startText = 'Купить',
                boughtText = 'Выбрано',
                order_items = typeof post_products !== typeof undefined && !status_send ? JSON.parse(post_products) : [];
        
        holder.on("click", ".buy_btn", function () {
            if (!$(this).hasClass("active")) {
                $(this).text(boughtText);
                $(this).addClass("active");
                var item = {};
                var item_holder = $(this).closest(".product");
                item.id = item_holder.data('item');
                item.name = item_holder.find('.title').text();
                item.price = item_holder.find('.price em:first').text().replace(/\s/, "");
                order_items.push(item);
                drawCartItems();
            }
        });

        cart.on("click", ".delete_item", function () {
            for (var i in order_items) {
                if (order_items[i].name == $(this).closest('.simple').find('.item_name').text()) {
                    $("[data-item=" + order_items[i].id + "]").find(".buy_btn").removeClass("active").text(startText);
                    order_items.splice(i, 1);
                }
            }
            drawCartItems();
        });

        var drawCartItems = function () {
            var sum = 350;
            car_items.find("li").not($("li.static")).remove();
            for (var i in order_items) {
                car_items.prepend('<li class="simple">' +
                        '<div class="item_name">' + order_items[i].name + '</div>' +
                        '<i class="fa fa-times delete_item"></i>' +
                        '</li>');
                sum += +order_items[i].price;
            }
            cart.find(".total_price").text(sum.toLocaleString({useGrouping: true}));

            cart.find("input[name=products]").val(JSON.stringify(order_items));

            (order_items.length == 0 ?
                    (cart.fadeOut(), $("html, body").animate({scrollTop: holder.offset().top}, 600)) :
                    (cart.fadeIn(), $("html, body").animate({scrollTop: cart.offset().top}, 600))
                    );
            window.order_items = order_items;
        };
        if (!status_send && order_items.length > 0)
            drawCartItems();
    }
    /* END PRESENT CARDS*/
    
    $('#bookgift-form').submit(function () {
        if (preventMultipleClicks)
            return false;
        preventMultipleClicks = true;

        $('#mytxt').val(my_text);
        
        var bookName = stripHTML($('#bookgift-name').val()),
            bookPhone = stripHTML($('#bookgift-phone').val()),
            bookAddress = stripHTML($('#bookgift-address').val());
        
        if (bookName != '') {
            if (bookPhone != '') {
                if (bookAddress != '') {
                    return true;
                } else {
                    alert('Зполните пожалуйста поле "Адрес"');
                    $('#bookgift-address').html(bookAddress).focus();
                    return false;
                }
            } else {
                alert('Зполните пожалуйста поле "Телефон"');
                $('#bookgift-phone').html(bookPhone).focus();
                return false;
            }
        } else {
            alert('Зполните пожалуйста поле "Имя"');
            $('#bookgift-name').html(bookName).focus();
            return false;
        }

        if (typeof order_items !== typeof undefined) {
            if (order_items.length > 0) {

            }
        }

    });

    // after bookinng giftcards
    
    

    $('.priceTbl').tooltip();

    $(window).load(function () {
        if (typeof ordergiftcard != 'undefined' && ordergiftcard == 1) {
//            yaCounter25221941.reachGoal("ordergiftcard");
            ga("gtm1450431798761.send", "event", "order", "giftcard");
        }
    });

    $('.city-select').on('show.bs.dropdown', function () {
        var dropdown_menu = parseInt($('.city-select .dropdown-menu').width()), // -56px
                abtn_link = parseInt($('.ico-msq').outerWidth());

        if (dropdown_menu > abtn_link) {
            var imw = $('.ico-msq').width();
            $('.ico-msq').width(dropdown_menu - 16);
            var mr = $('.ico-msq').width() - imw;
            $('.city-select').css('margin-right', '-' + mr + 'px');
        }
    });

    if ($('.img-container').length > 0) {
        function setSize(resize) {
            var ww = $(window).width();
            if (ww > 1023) {
                $('.img-container').width(ww - 514);
            } else {
                $('.img-container').width('100%');
            }
        }

        setSize();
        window.addEventListener(orientationEvent, setSize);

        if ($('.img-container').length > 1) {
            var offset = 0;
            $('.img-container').each(function (index) {
                $(this).css('right', offset);
                offset = offset - $(this).width();
            });
            // $( ".img-container" ).animate({    right: "-=1151" }, 2000);
        }
    }

    $('#myModalAuth').on('show.bs.modal', function () {
        if ($(window).width() < 768) {
            window.location.href = '/user/login';
            return false;
        }
    });

    if ($('#winner').length > 0 && window.location.hash != "") {
        $('a[href="' + window.location.hash + '"]').click()
    }

    if ($('#rating_alltime').length > 0 && $('#rating_month').length > 0
            && (window.location.hash == "#rating_alltime" || window.location.hash == "#rating_month")
            ) {
        $('a[href="#rating"]').click();
        setTimeout(function () {
            $('a[href="' + window.location.hash + '"]').click();
        }, 200);
        fixRatingMostPhotos();

    }

    $('.map_info_head .close').click(function () {
        var w = $('.map_info').width();
        $('.map_info').animate({left: '-' + w + 'px'}, 500);
        return false;
    });

    $('.map_info_show a').click(function () {
        $('.map_info').animate({left: '0'}, 500);
        return false;
    });


    $("#winner").on("click", '.btn-month', function () {
        $('.btn-month').removeClass('active');
        var m = $(this).addClass('active').attr('data-month');
        $('.month-pane').hide();
        $('#month_' + m).fadeIn();
    });
    
    var fixRatingMostPhotos = function(){
        setTimeout(function(){
            var maxHeight = 0;
                $('.rating_photos.most .border').each(function () {
                    if (maxHeight < $(this).get(0).clientHeight)
                        maxHeight = $(this).get(0).clientHeight;
                });
                if (maxHeight > 0) {
                    $('.rating_photos.most .thumbnail.thumbnail-transp').css("max-height",maxHeight - 70);
                }
        },100);
        
        $(window).resize(function(){fixRatingMostPhotos();});
    };
    
    
    $(".tab-content").on("click", '.btn-period', function (e) {
        e.preventDefault();
        $('.btn-period').removeClass('active');
        var period = $(this).addClass('active').attr('data-period');
        $('.period-pane').hide();
        $('#rating_' + period).fadeIn();
        var maxHeight = 0;
        $('.rating_photos:not(.most) .border').each(function () {
            if (maxHeight < $(this).height())
                maxHeight = $(this).height();
        });
        if (maxHeight > 0) {
            $('.rating_photos:not(.most) .border').height(maxHeight);
        }
        fixRatingMostPhotos();
    });



    //$(window).click(function(e) {
    //    if($(e.target).attr("id") === "popup-nav-manipulation") {
    //        var $nav = $("#popup-nav");
    //        if ($nav.hasClass("hide-popup-nav")) {
    //            $nav.removeClass("hide-popup-nav");
    //        } else {
    //            $nav.addClass("hide-popup-nav");
    //        }
    //    }
    //});
    
    // fix modalbook height for desktop
    var fixHeightModalbook = function(){
        if($("#prev_books").size()>0){
            var ratio = 0.60;
            if($(window).width() >= 768){
                $(".row.ModalBook").each(function(){
                    var bh = $(this).find(".book_info").width();
                    var nh = bh * ratio;
                    $(this).find(".col-sm-6").height(nh);
                });
            }
        }
    };

    fixHeightModalbook();
    $(window).resize(function(){fixHeightModalbook()});
    
    var defineYT = false;
    var intervalYt = setInterval(function(){
        if(typeof YT !== typeof undefined){
            defineYT = true;
            clearInterval(intervalYt);
            startYT();
        }
    },100);
    var startYT = function(){
        YT.ready(function(){
            console.log("YT is ready");
            setTimeout(function(){
                var player, currentID;
                var onPlayerReady = function(){
                    $('#ModalYoutube').css({display:'none',left:'0px', width:'100%'})
                    $(".watch_video").click(function(){
                        $("#ModalYoutube").modal("show");
                        $('#ModalYoutube').on('hidden.bs.modal', function (e) {
                            player.stopVideo();
                        });
                        player.playVideo();
                    });
                    $("#ModalYoutube").on("click",".close", function(){
                        player.stopVideo();
                    });
                };
                var videoID = $("#ModalYoutube").data("youtube");
                if(videoID.length > 0){
                    player = new YT.Player("player", {
                        height: "100%",
                        width: "100%",
                        videoId: videoID,
                        events: {
                            "onReady": onPlayerReady,
                        }
                    });
                }
            },150);
        });
    };

    $(".championship").click(function(){
        $("#ModalChampionship").modal("show");
    });

    
    /*Leave review*/
    var reviewRating = $('input[name=rating]').val();
    var trackStarsStopped = false;
    
    $(".rating-stars").on("click", ".rating-icon",function(){
        var i = $(this).index();
        reviewRating = i + 1;
        trackStarsStopped = true;
        $(".rating-stars").find(".rating-icon").each(function(){
            $(this).removeClass("rating_open rating_close");
            if($(this).index() <= i){
                $(this).addClass("rating_open");
            } else {
                $(this).addClass("rating_close");
            }
        });
        $('input[name=rating]').val(reviewRating);
    });
    
    
    $(".rating-stars .rating-icon").hover(function(){
        var i = $(this).index();
        $(".rating-stars .rating-icon").removeClass("rating_open rating_close");
        $(".rating-stars").find(".rating-icon").each(function(){
            if($(this).index() <= i){
                $(this).addClass("rating_open");
            } else {
                $(this).addClass("rating_close");
            }
        });
    },function(){
        var i = $(this).index();
        if(!trackStarsStopped){
            i = reviewRating - 1;
        }
        $(".rating-stars .rating-icon").removeClass("rating_open rating_close");
        $(".rating-stars").find(".rating-icon").each(function(){
            if($(this).index() <= i){
                $(this).addClass("rating_open");
            } else {
                $(this).addClass("rating_close");
            }
        });
        trackStarsStopped = false;
    });
    
    $(".sendReview").click(function(){
        var form = $(this).closest(".form-review");
        if(reviewRating > 0 && $.trim(form.find("textarea").val()) != ''){
            form.submit();
        } else {
            if(form.find("textarea").val().length < 50){
                infoPopup.showMessage('Слишком короткий отзыв', 'warning', {'NO': 'Ок'}, function (result) {
                   infoPopup.hide(true);
                });
            } else if(reviewRating == 0){
                infoPopup.showMessage('Поставьте пожалуйста рейтинг', 'warning', {'NO': 'Ок'}, function (result) {
                    infoPopup.hide(true);
                });
            }
        }
    });
    
    $(".leave-review").click(function(){
        var quest_id = $(this).data("quest"),
            quest_type = $(this).data("quest-type");
        
        if(quest_id && quest_type){
            document.location.href = '/review/create/'+quest_type+'/'+quest_id;
        }
    });
    
    if(typeof notification_type !== typeof undefined && notification_type == 'login_needed'){
        $("#for-login-pl .enter-cabinet").click();
    }
    
    if(typeof notification_message !== typeof undefined && notification_message.length > 0){
        infoPopup.showMessage(notification_message, 'warning', {'NO': 'Ок'}, function (result) {
            infoPopup.hide(true);
            if(quest_id && quest_type){
                if(quest_type == 'quest' || quest_type == 'performance')
                    document.location.href = '/'+quest_type+'/'+quest_id+'#reviews';
                else{
                    document.location.href = '/'+quest_type+'#reviews';
                }
            } else {
                document.location.href = "/";
            }
        });
    }
    if($(".review_paginator")>0){
        $(".review_paginator").on("click","page",function(){
            var page = $(this).data("page");
            $(".reviews_group_holder").addClass("hidden");
            $(".reviews_group_holder:eq("+page+")").removeClass("hidden");
            $(".review_paginator .page").removeClass("active");
            $(this).addClass("active");
        });
    }
    
    $("#reviews").on("click", ".glyphicon-remove", function(){
        var id = $(this).closest(".single-review").data("review"),
            self = $(this).closest(".single-review");
            
        infoPopup.showMessage("Удалить отзыв?", 'warning', {'YES':'Да','NO': 'Нет'}, function (result) {
            if(result){
                var postUrl = '/review/delete';
                var from = "/" + quest_type + "/getreviews/" + quest_id;
                $.post(postUrl, {id:parseInt(id)}, function(res){
                    infoPopup.hide(true);
                    var response = JSON.parse(res);
                    if(response.status){
                        $.get(from, {}, function (res) {
                            $("#reviews").html(res);
                            var ReviewSlider = jQuery.extend(true, {}, QuestSlider);
                            ReviewSlider.init({
                                container:'#reviews',
                                paginator: '.indicators-review',
                                isAuto:false,
                            }); 
                        });
                    } else {
                        console.log(response.message);
                    }
                });
            } else {
                infoPopup.hide(true);
            }
        });
    });
    /*End leave review*/
    
    
});

//$(window).load(function() {
$(document).ready(function(){
    if ($.cookie("last_seance_click_date") && $.cookie("last_seance_click_time")) {
        $('.btn.btn-q[data-ymd="' + $.cookie("last_seance_click_date") + '"][data-time="' + $.cookie("last_seance_click_time") + '"]').click();
        $.removeCookie("last_seance_click_date");
        $.removeCookie("last_seance_click_time");
    }
});

/*
* Site alert popup
*/
var infoPopup = {
   id: "popup_message",
   callback: null,
   type: null,
   // message String
   // type Enum ('warning', 'error' or ''), Defa
   // buttons Object, Default:{}
   // callback Function, Returns your callback with answer parameter (false by default)
   showMessage: function (message, type, buttons, callback) {

       if (typeof type != 'undefined' && type.length > 0) {
           this.type = type;

           switch (type) {
               case 'warning':
                   title = '<div class="sign-row"><img src="/images/cancel_booking.png"></div>';
                   break;
               case 'error':
                   title = '<div class="title">Ошибка!</div>';
                   break;
               default:
                   title = '';
           }
       } else
           title = '';

       var type = typeof type != 'undefined' && type.length > 0 ? type : '';

       var popup = $('<div class="holder-popup ' + type + '"><div id="message">' + title + '</div></div>'); //<a class="close">X</a>

       if ($("body").find("#" + this.id).size() < 1) {
           $("body").append('<div id="' + this.id + '"></div><div id="overlay"></div>');
       }

       popup.find("#message").append("<span>" + message + "</span>");

       if ($(buttons).size() > 0) {
           var buttons_obj = $('<div class="info_buttons"></div>');
           for (var key in buttons) {
               buttons_obj.append($('<div class="' + key + '_btn">' + buttons[key] + '</div>'));
           }
           popup.find("#message").append(buttons_obj);
       }

       $("#" + this.id).append(popup);

       $("#" + this.id).fadeIn(300);
       this.bindEvents();

       if (typeof callback == 'function')
           this.callback = callback;
   },
   hide: function (skip) {
       
       var self = this;
       $("#overlay").remove();
       $("#" + this.id).fadeOut(300, function () {
           $("#" + this.id).remove();

           if (typeof self.callback == 'function') {
               if (typeof skip == 'undefined' || !skip)
                   self.callback();

               self.callback = null;
           }

       });
   },
   bindEvents: function () {
       var self = this,
               check = false;

       $("#" + this.id + ' .close').bind("click", function (e) {
           e.preventDefault();
           if ((self.type != null || typeof self.type !== typeof undefined) && self.type != '') {
               self.hide(false);
           } else {
               self.hide(false);
           }
       });

       $(document).find("#" + this.id).unbind("click").bind("click", function (e) {
           if ($(e.target).attr('id') != 'message' && !$(e.target).hasClass('close')) {
               if (self.type != null || typeof self.type !== typeof undefined) {
                   if ($(e.target).hasClass('YES_btn') || $(e.target).hasClass('NO_btn')) {
                       self.callback($(e.target).hasClass('YES_btn'));
                   } else
                       self.hide(true);
                   ;
               } else
                   self.hide(true);
           }
       });

   },
};

window.infoPopup = infoPopup;

var ways_by_car = [],
        ways_by_foot = [],
        map;
function init_map() {
    var addr_end = 'Лужнецкая набережная, 2/4 строение 37, Москва, Россия',
            image_end = '/img/logo_cq.svg',
            image_start = '/img/logo_metro.svg',
            center_end, center_start,
            geocoder = new google.maps.Geocoder(),
            latlngbounds = new google.maps.LatLngBounds(),
            directionsService = new google.maps.DirectionsService;
    map_style =
            [{
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [{
                            "visibility": "on"
                        }, {
                            "color": "#475a8b"
                        }]
                }, {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                            "color": "#6782ba"
                        }]
                }, {
                    "featureType": "water",
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                            "visibility": "off"
                        }]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [{
                            "color": "#3b4360"
                        }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                            "visibility": "off"
                        }]
                }, {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.fill",
                    "stylers": [{
                            "visibility": "on"
                        }, {
                            "color": "#3b4360"
                        }]
                }, {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                            "color": "#475a8b"
                        }]
                }, {
                    "featureType": "road",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                            "visibility": "off"
                        }]
                }, {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [{
                            "color": "#6e89c0"
                        }]
                }, {
                    "featureType": "road",
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                            "visibility": "on"
                        }, {
                            "color": "#3b4360"
                        }]
                }, {
                    "featureType": "poi",
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                            "color": "#3b4360"
                        }]
                }, {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                            "color": "#ccddfb"
                        }]
                }, {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                            "color": "#ccddfb"
                        }]
                }, {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                            "color": "#3b4360"
                        }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                            "color": "#b0c5eb"
                        }]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                            "color": "#2f3756"
                        }]
                }, {
                    "featureType": "transit",
                    "elementType": "geometry.fill",
                    "stylers": [{
                            "color": "#2f3756"
                        }]
                }, {
                    "featureType": "road",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                            "visibility": "off"
                        }]
                }];
    lineSymbol = {
        path: google.maps.SymbolPath.CIRCLE,
        strokeOpacity: 1,
        scale: 2,
        fillColor: '#ffffff',
        fillOpacity: 1,
        strokeWeight: 0,
    },
            myMapOptions = {
                zoom: 15,
                scrollwheel: false,
                disableDefaultUI: true,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.RIGHT_TOP
                },
                center: {lat: 55.711498, lng: 37.567898},
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: map_style
            },
    map = new google.maps.Map(document.getElementById("gmap_canvas"), myMapOptions),
            // маркер квестов
            marker_end = new google.maps.Marker({
                position: {lat: 55.715581, lng: 37.572087},
                map: map,
                title: addr_end,
                icon: image_end
            }),
            // маркер метро
            marker_start = new google.maps.Marker({
                position: {lat: 55.711497, lng: 37.561201},
                map: map,
                title: 'Метро',
                icon: image_start
            });

    // путь на машине 1
    ways_by_car.push(
            new google.maps.Polyline({path:
                        [
                            new google.maps.LatLng(55.720365, 37.560697),
                            new google.maps.LatLng(55.718991, 37.564915),
                            new google.maps.LatLng(55.717746, 37.568456),
                            new google.maps.LatLng(55.717609, 37.568640),
                            new google.maps.LatLng(55.717446, 37.568533),
                            new google.maps.LatLng(55.716542, 37.567296),
                            new google.maps.LatLng(55.716563, 37.566026),
                            new google.maps.LatLng(55.715459, 37.564191),
                            new google.maps.LatLng(55.712643, 37.569189),
                            new google.maps.LatLng(55.712477, 37.569411),
                            new google.maps.LatLng(55.712823, 37.571184),
                            new google.maps.LatLng(55.713026, 37.571882),
                                    // new google.maps.LatLng(55.713198, 37.572524),
                                    // new google.maps.LatLng(55.713217, 37.572532),
                                    // new google.maps.LatLng(55.713219, 37.572522),
                        ],
                strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2,
            })
            );

    // путь на машине 2
    ways_by_car.push(
            new google.maps.Polyline({path:
                        [
                            new google.maps.LatLng(55.716642, 37.579451),
                            new google.maps.LatLng(55.714705, 37.575916),
                            new google.maps.LatLng(55.714541, 37.575704),
                            new google.maps.LatLng(55.714297, 37.575598),
                            new google.maps.LatLng(55.713685, 37.573937),
                            // new google.maps.LatLng(55.713217, 37.572532),
                            new google.maps.LatLng(55.713352, 37.572957),
                        ],
                strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2
            })
            );
    // путь на машине 3
    ways_by_car.push(
            new google.maps.Polyline({path:
                        [
                            new google.maps.LatLng(55.713401, 37.578305),
                            new google.maps.LatLng(55.714153, 37.577268),
                            new google.maps.LatLng(55.714914, 37.576190),
                            new google.maps.LatLng(55.715586, 37.575041),
                            new google.maps.LatLng(55.715751, 37.574926),
                            new google.maps.LatLng(55.716492, 37.573495),
                            new google.maps.LatLng(55.717334, 37.571453),
                            new google.maps.LatLng(55.719165, 37.565559),
                            new google.maps.LatLng(55.719160, 37.565329),
                            new google.maps.LatLng(55.719016, 37.565056),
                            new google.maps.LatLng(55.718812, 37.564941),
                            new google.maps.LatLng(55.718618, 37.565082),
                            new google.maps.LatLng(55.718538, 37.565356),
                            new google.maps.LatLng(55.717697, 37.568246),
                            new google.maps.LatLng(55.717606, 37.568411),
                            new google.maps.LatLng(55.717479, 37.568437),
                            new google.maps.LatLng(55.717374, 37.568415),
                        ],
                strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2 //78c3e7
            })
            );
    // Проезд по территории
    ways_by_car.push(
            new google.maps.Polyline({
                path: [
                    new google.maps.LatLng(55.713220, 37.572491),
                    new google.maps.LatLng(55.713365, 37.572341),
                    new google.maps.LatLng(55.713561, 37.572081),
                    new google.maps.LatLng(55.713845, 37.571561),
                    new google.maps.LatLng(55.714015, 37.571291),
                    new google.maps.LatLng(55.714126, 37.571176),
                    new google.maps.LatLng(55.714220, 37.571187),
                    new google.maps.LatLng(55.714328, 37.571280),
                    new google.maps.LatLng(55.714404, 37.571390),
                    new google.maps.LatLng(55.714588, 37.571691),
                    new google.maps.LatLng(55.714706, 37.571847),
                    new google.maps.LatLng(55.714763, 37.571880),
                    new google.maps.LatLng(55.714829, 37.571832),
                    new google.maps.LatLng(55.715053, 37.571526),
                    new google.maps.LatLng(55.715537, 37.572309),
                ],
                strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2
            })
            );
    // соединим стрелки
    ways_by_car.push(
            new google.maps.Polyline({
                path: [
                    new google.maps.LatLng(55.713327, 37.572883),
                    new google.maps.LatLng(55.713053, 37.571975),
                ],
                strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2
            })
            )
    // Create the polyline and add the symbol via the 'icons' property.
    ways_by_car.push(
            new google.maps.Polyline({
                path: [
                    new google.maps.LatLng(55.713352, 37.572957),
                    // new google.maps.LatLng(55.713219, 37.572522), // стрелки впритык друг к другу
                    new google.maps.LatLng(55.713327, 37.572883),
                ],
                icons: [{
                        icon: {
                            path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
                        },
                        offset: '100%'
                    }],
                strokeColor: "#ffffff",
                strokeWeight: 4
            })
            );
    // Create the polyline and add the symbol via the 'icons' property.
    ways_by_car.push(
            new google.maps.Polyline({
                path: [
                    new google.maps.LatLng(55.713026, 37.571882),
                    // new google.maps.LatLng(55.713219, 37.572522),// стрелки впритык друг к другу
                    new google.maps.LatLng(55.713053, 37.571975),
                ],
                icons: [{
                        icon: {
                            path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
                        },
                        offset: '100%'
                    }],
                strokeColor: "#ffffff",
                strokeWeight: 4
            })
            );

    //1
    ways_by_car.push(
            new google.maps.InfoWindow({
                disableAutoPan: true,
                //maxWidth: 150,
                position: new google.maps.LatLng(55.716683, 37.579464),
                content: "<div style='color:#000; font-size:11px; position: relative; left:10px;'>" +
                        "Заезд с Фрунзенской набережной</div>"
            })
            );
    //2
    ways_by_car.push(
            new google.maps.InfoWindow({
                disableAutoPan: true,
                //maxWidth: 150,
                position: new google.maps.LatLng(55.713546, 37.578069),
                content: "<div style='color:#000; font-size:11px; position: relative; left:10px;'>" +
                        "Заезд с ТТК (внешняя сторона)</div>"
            })
            );
    //3
    ways_by_car.push(
            new google.maps.InfoWindow({
                disableAutoPan: true,
                //maxWidth: 150,
                position: new google.maps.LatLng(55.719209, 37.564379),
                content: "<div style='color:#000; font-size:11px; position: relative; left:10px;'>" +
                        "Заезд с ТТК (внутренняя сторона)</div>"
            })
            );



    // Проход по территории пешком
    ways_by_foot.push(
            new google.maps.Polyline({path:
                        [
                            new google.maps.LatLng(55.713220, 37.572491),
                            new google.maps.LatLng(55.713365, 37.572341),
                            new google.maps.LatLng(55.713561, 37.572081),
                            new google.maps.LatLng(55.713845, 37.571561),
                            new google.maps.LatLng(55.714015, 37.571291),
                            new google.maps.LatLng(55.714126, 37.571176),
                            new google.maps.LatLng(55.714220, 37.571187),
                            new google.maps.LatLng(55.714328, 37.571280),
                            new google.maps.LatLng(55.714404, 37.571390),
                            new google.maps.LatLng(55.714588, 37.571691),
                            new google.maps.LatLng(55.714706, 37.571847),
                            new google.maps.LatLng(55.714763, 37.571880),
                            new google.maps.LatLng(55.714829, 37.571832),
                            new google.maps.LatLng(55.715053, 37.571526),
                            new google.maps.LatLng(55.715537, 37.572309),
                        ],
                strokeOpacity: 0,
                icons: [{
                        icon: lineSymbol,
                        offset: '0',
                        repeat: '10px'
                    }],
            })
            );

    ways_by_foot.push(
            new google.maps.Polyline({path:
                        [
                            new google.maps.LatLng(55.711490000000005, 37.561330000000005),
                            new google.maps.LatLng(55.71190000000001, 37.56179),
                            new google.maps.LatLng(55.711420000000004, 37.56273),
                            new google.maps.LatLng(55.711580000000005, 37.564220000000006),
                            new google.maps.LatLng(55.71172000000001, 37.56524),
                            new google.maps.LatLng(55.71190000000001, 37.56633),
                            new google.maps.LatLng(55.71204, 37.56709),
                            new google.maps.LatLng(55.71202, 37.56725),
                            new google.maps.LatLng(55.71206, 37.567420000000006),
                            new google.maps.LatLng(55.712210000000006, 37.56815),
                            new google.maps.LatLng(55.71269, 37.57054),
                            new google.maps.LatLng(55.712820, 37.571188),
                            new google.maps.LatLng(55.713220, 37.572491),
                        ],
                strokeOpacity: 0,
                geodesic: true,
                icons: [{
                        icon: lineSymbol,
                        offset: '0',
                        repeat: '10px'
                    }],
            })
            );

    /* Маршрут пешком */
    $.each(ways_by_foot, function (i, ways) {
        ways.setMap(map);
    });
    
}

// google.maps.event.addDomListener(window, "load", init_map);

$(function () {

    var a_walk = $('a[href="#walk"]'),
            a_bycar = $('a[href="#bycar"]');

    /* Переключаем маршруты на карте */
    a_walk.on('shown.bs.tab', function (e) {

        $.each(ways_by_car, function (i, ways) {
            ways.setMap(null);
        });
        $.each(ways_by_foot, function (i, ways) {
            ways.setMap(map);
        });
    });
    /* Переключаем маршруты на карте */
    a_bycar.on('shown.bs.tab', function (e) {

        $.each(ways_by_foot, function (i, ways) {
            ways.setMap(null);
        });

        $.each(ways_by_car, function (i, way) {
            // console.log(way, way.disableAutoPan);
            way.setMap(map);
            // if (typeof(way.disableAutoPan) != 'undefined') {}
        });
        $('.gm-style-iw div').css('overflow', 'visible');
    });


    /*$('#show-menu').click(function() {
     
     if (document.body.clientWidth < 768) {
     if ($('#for-select-city').text() == '') {
     $('.city-select').show().appendTo('#for-select-city');
     $('#for-city').html('');
     }
     
     if ($('#for-login').text() == '') {
     $('#for-login-pl .btn').show().css('display', 'inline-block').appendTo('#for-login');
     $('#for-login-pl').html('');
     }
     } else {
     if ($('#for-login-pl').text() == '') {
     $('#for-login-pl .btn').hide().appendTo('#for-login-pl');
     $('#for-login').html('')
     }
     if ($('#for-city').text() == '') {
     $('.city-select').hide().appendTo('#for-city');
     $('#for-select-city').html('');
     }
     }
     
     if ($('#myModalMenu #for-menu').text() == '') {
     $('#top_menu').show().appendTo('#myModalMenu #for-menu');
     }
     
     $('#myModalMenu').modal('toggle');
     });*/

    if ($("#carouselPhoto").size() > 0) {
        var resize = function () {
            var ratio = 675 / 1200;
            var ww = $(window).width(),
                    wh = $(window).height();


            if (window.location.pathname.indexOf('quest') < 0) {
                if (window.location.pathname.indexOf('performance'))
                    return;
            }

//            box-content


            var gap = 1005, gap2 = 1440;
            if (ww <= gap) {
//                var newHeight = ww * ratio;
//                if(newHeight <= 410){
//                    newHeight = 410;
//                    $("#carouselPhoto .item").css({
//                        'background-size': 'auto 100%'
//                    });
//                } else {
//                    $("#carouselPhoto .item").css({
//                        'background-size': '100% auto'
//                    });
//                }
//                $(".quest_description").css("margin-top", newHeight - 90 + "px");
//                $("#carouselPhoto").height(newHeight);
                if (window.location.pathname.indexOf('action') >= 0) {
//                    $("#carouselPhoto.action .carousel-inner").height(400);
//                    $("#carouselPhoto.action .carousel-inner>.item>img").height(400);
//                    $("#carouselPhoto.action .carousel-inner>.item>img").css("width","100%");

                }
            } else {
                if (ww > wh) {
                    $("#carouselPhoto .item").css({
                        'background-size': 'auto 100%'
                    });
                } else {
                    $("#carouselPhoto .item").css({
                        'background-size': 'auto 100%'
                    });
                }
                var qh = $("#quest_description").get(0).clientHeight;

                //$("#quest_description").css("margin-top", (wh - 90 - (ww > gap && ww < gap2 ? 0 : qh)) + "px");
                //$(".img-container").height(wh - (ww > gap && ww < gap2 ? 0 : qh));
                $(".img-container .item").css("background-size", "cover");
            }

        };
        resize();
        $(window).resize(function () {
            resize();
        });
    }

//    $('.quests_nav a').unbind("click tap").bind("click tap", function(){
//        console.log("click");
//        document.location.href = $(this).attr("href");
//    });

    QuestSlider.init({container:'#carouselPhoto', isAuto:true});
});


$(document).ready(function(){
    if ($(".giftcards").size() > 0) {
        // if products bought success
        if(status_send && typeof post_products !== typeof undefined){
            var prods = JSON.parse(post_products);
            if(prods.length > 0){
                window.dataLayer = window.dataLayer || [];
                var products = [], revenue = 0, 
                orderRandom = 'giftcardOrder'+ (new Date().getTime());
                for(var i in prods){
                    products.push({
                        'name': prods[i].name,
                        'id': "giftcard" + (prods[i].id+1),
                        'name': prods[i].name,
                        'price': prods[i].price,
                        'category': prods[i].name.split(" на")[0],
                        'quantity': 1
                    });
                    revenue += +prods[i].price;
                }
                dataLayer.push({
                    'event' : 'btnOrderCard',
                    'event-action': 'giftcards',
                    'ecommerce' : {
                        'purchase' : {
                            'actionField' : {
                                'id': orderRandom,
                                'revenue' : revenue
                            },
                            'products' : products
                        }
                    }
                });
            }
        }
    }

    $('.players_count-select li a').on('click', function() {
        var count = parseInt($(this).html());
        $('.is_action .players_count-select button .dropdown-content').html(count).click();

        if($.data($('#myModalBook')[0]).book_data && $.data($('#myModalBook')[0]).book_data.price) {
            var price = parseInt($.data($('#myModalBook')[0]).book_data.price);
        }

        if(count > 4) {
            if (price < 5000 || (price == '5000' && $.data($('#myModalBook')[0]).book_data.day != 'пятница')) {
                price += (count - 4) * 500;
            } else {
                price += (count - 4) * 1000;
            }
        }

        if(count > 5) {
            $('.players_count-hint').show();
        } else {
            $('.players_count-hint').hide();
        }

        $('.total_price .price_content').html(price);


        return false;
    })
});

var formatMoney = function(val, c, d, t){
    var n = val,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

$(document).ready(function () {
    // Smooth scroll to shedule
    $(".js-scroll-to-shedule").click(function (e) {
        e.preventDefault();

        $('body').animate({
            scrollTop: $('.twotab-wrap').offset().top
        }, 1100);
    });
});