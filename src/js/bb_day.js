var Holidays = Backbone.Collection.extend({model: Day});
var PromoDays = Backbone.Collection.extend({model: Day});
var PromoDay = Backbone.Model.extend({
    model: Day,
    defaults: {
        day: 0,
        price_am: 0,
        price_pm: 0,
        quest_id: 0
    },
});


var DayView = Backbone.View.extend({
    className: 'btn-group',
    template: _.template(
            '<a type="button" class="text-center btn btn-default btn-day">' +
            '<span><%= day %></span>' +
            '<small><%= dayOfTheWeek %></small>' +
            '<small><%= month %></small>' +
            '<span class="badge"><%= amount %></span>' +
            '</a>'
            ),
    events: {
        "click": "activate",
    },
    initialize: function () {
        this.model.on('change', this.render, this);
        this.render();
    },
    render: function () {
        var day = this.model;

        this.$el
                .html(this.template(day.attributes));

        var btn = $('a', this.$el)
                .attr('href', '#day/' + day.get('ymd'));


        if (day.get('active'))
            btn.addClass('btn-success').attr("disabled", "disabled");
        else
            btn.removeClass('btn-success').removeAttr('disabled');


        if (day.get('today'))
            btn.addClass('active');
        else
            btn.removeClass('active');


        if (day.get('weekend') || day.get('holiday'))
            btn.addClass('btn-warning');
        else
            btn.removeClass('btn-warning');
        
        this.getBookings();

        return this;
    },
    getBookings: function () {
        var ymd = this.model.get('ymd'),
                m = this.model,
                v = this;
        $('.badge', v.$el).html( m.get('badge') );
    },
    activate: function () {
        app.days.setDate(this.model.get('ymd'));

        this.model.collection.removeActive();
        this.model.set('active', true);
    }
});

var Day = Backbone.Model.extend({
    defaults: {
        current: false,
        active: false,
        ymd: 0,
        day: 0,
        dayOfTheWeek: '',
        month: '',
        month_number: 0,
        weekend: false,
        holiday: false,
        Y: 2015,
        amount: 0,
        m: 00,
        d: 00,
        badge: 0,
    },
    initialize: function () {
        this.view = new DayView({model: this});
        this.on('change:active change:holiday change:amount', function () {
            this.view.render();
        });

        this.on('destroy', this.modelDestroy, this)
    },
    modelDestroy: function (a, b, c) {

    },
});

var Days = Backbone.Collection.extend({
    model: Day,
    days_of_week: days_of_week,
    months: months,
    period: 14,
    day_offset: -2,
    holidays_ready: false,
    promo_ready: false,
    interval_started: false,
    last_autoupdate: null,
    autoupdate_period: 30, // 30seconds
    active_day: null,
    stop_autoupdate: false,
    current_city: null,
    initialize: function (models, options) {
        this.app = options.app;
        this.deferred_holidays = $.Deferred();
        this.deferred_promo = $.Deferred();
        this.deferred = $.Deferred();
    },
    getAll: function (offset) {
        var date = new Date(),
            mondays = new Date(date),
            endPeriod = new Date(date),
            self = this;

        var timeperiod = this.period * 3600 * 24 * 1000;
        this.day_offset = offset || (date.getDay() > 0 ? date.getDay() - 1 : 6);

        mondays.setDate(date.getDate() - this.day_offset);
        endPeriod.setDate(date.getDate() - this.day_offset + this.period - 1);

        var from = mondays.getFullYear() + '-' +
                (mondays.getMonth() + 1 < 10 ? '0' + (mondays.getMonth() + 1) : mondays.getMonth() + 1) + '-' +
                (mondays.getDate() < 10 ? '0' + mondays.getDate() : mondays.getDate());
        var till = endPeriod.getFullYear() + '-' +
                (endPeriod.getMonth() + 1 < 10 ? '0' + (endPeriod.getMonth() + 1) : endPeriod.getMonth() + 1) + '-' +
                (endPeriod.getDate() < 10 ? '0' + endPeriod.getDate() : endPeriod.getDate());

        $.get('/booking/getbyperiod?from=' + from + '&till=' + till + '&city=' + city_id + '&readonly='+readonly, function (r) {
            if(window.popup_is_opened) {
                return false;
            }

            $.each(r.bookings['by_date'], function(date, date_bookings) {
                $.each(date_bookings, function(key, booking) {
                    if(booking['quest_id'] > 0) {
                        r.bookings['by_quest'][booking['quest_id']].push(booking);
                    } else if(booking['action_id'] > 0) {
                        r.bookings['by_action'][booking['action_id']].push(booking);
                    } else if(booking['performance_id'] > 0) {
                        r.bookings['by_performance'][booking['performance_id']].push(booking);
                    } else if(booking['vr_id'] > 0) {
                        r.bookings['by_vr'][booking['vr_id']].push(booking);
                    }
                })
            });
            self.bookings = r.bookings['by_date'];
            self.socials = r.socials;
            self.holidays = new Holidays();
            self.current_city = r.current_city;
            self.cities = r.cities;
            $.each(r.holidays, function(){
                var day = new Day({ymd:this.holiday_date});
                self.holidays.add(day);
            });
            self.promodays = new PromoDays();
            $.each(r.promodays, function(){
                if(this.price_am && this.price_pm){
                    var day = new PromoDay({
                        ymd:this.day,
                        day: this.day,
                        price_am: this.price_am,
                        price_pm: this.price_pm,
                        quest_id: this.quest_id
                    });
                    self.promodays.add(day);
                }
            });
            
            //quest collection
            self.quests = new Quests([],{app:self.app});
            $.each(r.quests, function(){
                this.object_type = 'quest';
//                console.log(this.price_schedule_workday);
                //parsePriceAndSchedule described in the top of admin_main.js.
                if (!this.price_schedule_weekend)
                    this.price_schedule_weekend = '20,00:00,';
                var result_day_schedule = parsePriceAndSchedule(this.price_schedule_weekend);
                var alignedArrayForSchedule = [];
                var j = 0;
                for (var i in result_day_schedule) {
                    alignedArrayForSchedule[j] = i;
                    j++;
                }
                this.seanses = alignedArrayForSchedule;

                this.bookings = r.bookings['by_quest'];
                this.socials = self.socials;
                var quest = new Quest( this );
                self.quests.add(quest);
            });
            // add actions to collection
            $.each(r.actions, function(){
                this.object_type = 'action';
                if (!this.price_schedule_weekend)
                    this.price_schedule_weekend = '20,00:00,';
                var result_day_schedule = parsePriceAndSchedule(this.price_schedule_weekend);
                var alignedArrayForSchedule = [];
                var j = 0;
                for (var i in result_day_schedule) {
                    alignedArrayForSchedule[j] = i;
                    j++;
                }
                this.seanses = alignedArrayForSchedule;
                this.bookings = r.bookings['by_action'];
                this.socials = self.socials;
                var quest = new Quest( this );
                self.quests.add(quest);
            });
            // add performances to collection
            $.each(r.performances, function(){
                this.object_type = 'performance';
                //parsePriceAndSchedule described in the top of admin_main.js.
                var result_day_schedule = parsePriceAndSchedule(this.price_schedule_workday);
                var alignedArrayForSchedule = [];
                var j = 0;
                for (var i in result_day_schedule) {
                    alignedArrayForSchedule[j] = i;
                    j++;
                }
                this.seanses = alignedArrayForSchedule;
                this.bookings = r.bookings['by_performance'];
                this.socials = self.socials;
                var quest = new Quest( this );
                self.quests.add(quest);
            });
            $.each(r.vrs, function(){
                this.object_type = 'vr';
//                this.seanses = this.schedule.split(",");
                var result_day_schedule = parsePriceAndSchedule(this.price_schedule_weekend);
                var alignedArrayForSchedule = [];
                var j = 0;
                for (var i in result_day_schedule) {
                    alignedArrayForSchedule[j] = i;
                    j++;
                }
                this.seanses = alignedArrayForSchedule;
                this.bookings = r.bookings['by_vr'];
                this.socials = self.socials;
                var quest = new Quest( this );
                self.quests.add(quest);
            });
            //rendering
            self.quests.render();

            self.render_top_days(this.day_offset);
            Preloader.hide();
        });
    },
    interval:function(){
        var self = this;
        if(self.interval_started) 
            return;
        self.interval_started = true;
        console.log("interval starting...");
        setTimeout(function(){
            window.interval_resource = setInterval(function(){
                if(window.popup_is_opened){
                    clearInterval(window.interval_resource);
                    return;
                }
                if(self.last_autoupdate == null){
                    self.last_autoupdate = new Date().getTime();
                    self.getAll(self.day_offset);
                } else {
                    var now = new Date(); 
                    if(now.getTime() < self.last_autoupdate + self.autoupdate_period * 1000){
                        // wait
                    } else {
                        self.last_autoupdate = now.getTime();
                        self.getAll(self.day_offset);
                    }
                }
            },200);
        },self.autoupdate_period * 100);
    },
    render_top_days: function (day_offset) {
        var today_date = new Date(),
            active_date = new Date(),
            mondays = new Date(),
            i = 0,
            self = this,
            today = null,
            firstMonday = null;
            
        if (day_offset == -2) {
            self.app.workspace.navigate("", {trigger: false});
        }

        this.each(function(m){
            m.view.remove();
        });
        this.reset();

        this.day_offset = day_offset || this.day_offset;
        mondays.setDate(today_date.getDate()- this.day_offset);
        var found_active_day = false;
        while (i < this.period) {
            i++;
            var y = mondays.getFullYear(),
                m = ((mondays.getMonth() + 1) < 10) ? ('0' + (mondays.getMonth() + 1)) : mondays.getMonth() + 1,
                d = (mondays.getDate() < 10) ? ('0' + mondays.getDate()) : mondays.getDate();
            
            var day_bookings = self.bookings[y + '' + m + '' + d],
                badge = 0;
            if(typeof day_bookings !== typeof undefined && self.quests.length > 0){
                $.each(day_bookings, function(){
                    if(this.name != 'CQ') badge++;
                });
            }
            
            var day = new Day({
                'dayOfTheWeek': this.days_of_week[mondays.getDay()],
                'day': mondays.getDate(),
                'month': this.months[mondays.getMonth()],
                'today': (today_date.getTime() == mondays.getTime()) ? true : false,
                'active': (active_date.getTime() == mondays.getTime()) ? true : false,
                'weekend': (mondays.getDay() == 0 || mondays.getDay() == 6) ? true : false,
                'Y': y,
                'm': m,
                'd': d,
                'ymd': y + '' + m + '' + d,
                'badge' : badge
            });
            
            if (today_date.getTime() == mondays.getTime()){
                today = day;
            }
            if(i == 1)
                firstMonday = day;
            
            self.add(day);
            
            var found = null;
            self.holidays_ready = true;
            if(self.holidays && self.holidays.length > 0){
                found = self.holidays.where({ymd: y + '' + m + '' + d});
                
                if(found && found.length > 0){
                    this.holidays_ready;
                    day.view.$el.attr('data-holiday', 1);
                    day.set('holiday', 1);
                }
            }
            
            self.promo_ready = true;
            if(self.promodays && self.promodays.length > 0){
                this.promo_ready;
            }
            
            if(self.active_day && self.active_day.get('ymd') == day.get('ymd'))
                found_active_day = true;
            
            mondays.setDate(mondays.getDate() + 1);
            
        }
                
        self.render();

        if(self.active_day && found_active_day){
            self.setDate(self.active_day.get('ymd'));
        }else if(today) {
            self.setDate(today.get('ymd'));
        }
        else
            self.setDate(firstMonday.get('ymd'));
    },

    render: function () {
        var days_container = $('#bb_days .btn-group'),
            self = this;

        days_container.html('');

        if(!readonly){
            $('<a class="text-center btn btn-default btn-day">' +
                '<span class="hi hi-chevron-left"></span>' +
                '</a>')
                .click(function () {
                    
                    self.getAll(self.day_offset + self.period);
                    $('.close-booking').click();
                    Preloader.show();
                })
                .appendTo(days_container);
        }
        

        this.each(function (model) {
            days_container.append(model.view.render().el);
        });

        if(!readonly){
            $('<a class="text-center btn btn-default btn-day">' +
                '<span class="hi hi-chevron-right"></span>' +
                '</a>')
                .click(function () {
                    if(readonly) return;
                    self.getAll(self.day_offset - self.period);
                    $('.close-booking').click();
                    Preloader.show();
                })
                .appendTo(days_container);
        }
    },
    setDayOff: function (active_day) {
        $('.today_is')
            .html(active_day.get('day') + ' ' + active_day.get('month') + '. ' + active_day.get('Y'));


        $('.setHoliday')
                .attr({
                    'data-date': active_day.get('ymd'),
                    'data-holiday': (active_day.get('holiday') || active_day.get('weekend')) ? 1 : 0
                });


        if (active_day.get('holiday') || active_day.get('weekend')) {

            $('.setHoliday').addClass('hi-star').removeClass('hi-star-empty')
                .attr('title', 'Сделать рабочим');
            $('.block-title h2').attr('title', 'Выходной день');

        } else {

            $('.setHoliday').addClass('hi-star-empty').removeClass('hi-star')
                    .attr('title', 'Сделать выходным');
            $('.block-title h2').attr('title', 'Рабочий день');

        }

        if(superuser != 1) {
            $('.setHoliday').hide();
        }

        $('.setHoliday, .block-title h2')
                .tooltip('destroy')
                .tooltip({container: 'body'});
    },
    removeActive: function () {
        this.each(function (model) {
            model.set('active', false);
        });
    },
    getActiveDate: function () {
        return this.find(function (m) {
            return m.get('active');
        });
    },
    setDate: function (ymd) {
        var self = this;
        var active_day = false;

        this.each(function (model) {
            if (model.get('ymd') == ymd) {
                model.set('active', true);
                active_day = model;
            } else {
                model.set('active', false);
            }
        });
        if(active_day){
            self.setDayOff(active_day);
            self.quests.each(function(quest){
                quest.setPrice(active_day);
            });
            self.last_autoupdate = new Date().getTime();
            self.active_day = active_day;
            self.interval();
        }
    }
});
